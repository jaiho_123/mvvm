package com.appeteria.mvvm_demo;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Random;

public class MainActivityViewModel extends AndroidViewModel {

    private String TAG = this.getClass().getSimpleName();
  //  private String myRandomNumber;
    MutableLiveData<String> myRandomNumber;

    public MainActivityViewModel(@NonNull Application application) {
        super( application );
    }

    public MutableLiveData<String> getNumber() {
        Log.i(TAG, "Get number");
        if (myRandomNumber == null) {
            myRandomNumber =new MutableLiveData<>();
            createNumber();
        }
        return myRandomNumber;
    }

    public void createNumber() {
        Log.i(TAG, "Create new number");
        Random random = new Random();
      //  myRandomNumber = "Number: " + (random.nextInt(10 - 1) + 1);
        myRandomNumber.setValue( "Number: " + (random.nextInt(10 - 1) + 1) );
    }

  @Override
    protected void onCleared() {
        super.onCleared();
        Log.i(TAG, "ViewModel Destroyed");
    }
}
