package com.appeteria.mvvm_demo;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView mTextView;
    Button generateNumber;
    private String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = findViewById(R.id.tvNumber);

        generateNumber = findViewById(R.id.generateNumber);


      //  MainActivityViewModel myData = new MainActivityViewModel();
         final MainActivityViewModel model = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        //String myRandomNumber = model.getNumber();

        model.getNumber().observe( this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                mTextView.setText(s);
                Log.i(TAG, "Update Number Set");
            }
        } );
       // mTextView.setText(myRandomNumber);
        generateNumber.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                model.createNumber();
            }
        } );

    }
}